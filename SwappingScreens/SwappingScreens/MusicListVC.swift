//
//  MusicListVC.swift
//  SwappingScreens
//
//  Created by Huzaifa Kausar on 7/13/17.
//  Copyright © 2017 Huzaifa Kausar. All rights reserved.
//

import UIKit

class MusicListVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
            view.backgroundColor = UIColor.blue
        
    }
    
    
    @IBAction func backbtnPressed(_ sender: UIButton) {
        dismiss(animated: true, completion: nil)
    }
    // for moving on 3rd screen without segue show and using show details intead we use these steps
    //1-- perform segue= after setting identifier from identifieer inspector and passing it into this func
    
    //2-- prepare for segue = where we pass destination screen playsongvc and if let helps error handling
    @IBAction func load3rdScreenPressed(_ sender: UIButton) {
      let songTitle = "Dhinchak Poja"
        
        performSegue(withIdentifier: "PlaySongVC", sender: songTitle)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? PlaySongVC{
        
            if let song = sender as? String {
            destination.selectedSong = song
            }
        }
    }
}
