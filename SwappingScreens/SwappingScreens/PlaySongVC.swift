//
//  PlaySongVC.swift
//  SwappingScreens
//
//  Created by Huzaifa Kausar on 7/13/17.
//  Copyright © 2017 Huzaifa Kausar. All rights reserved.
//

import UIKit

class PlaySongVC: UIViewController {

    @IBOutlet weak var songTitleLbl: UILabel!
    //selected song is pass on to musiclistvc to help move on to this screen on btnpressed3rdscreen
    private var _selectedSong : String!
    var selectedSong : String{
        get{
            return _selectedSong
        }set{
            _selectedSong = newValue
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Here we simpily set lbl text to selected song which has value dhinchak pooja in this case
        songTitleLbl.text = _selectedSong
      
    }
    
    @IBAction func backBtnPressed2(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}
