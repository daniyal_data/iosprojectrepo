//
//  ViewController.swift
//  RetroCalculator
//
//  Created by Huzaifa Kausar on 7/13/17.
//  Copyright © 2017 Huzaifa Kausar. All rights reserved.
//

import UIKit
import AVFoundation

class ViewController: UIViewController {
    
    @IBOutlet weak var outputLbl: UILabel!
    var btnSound : AVAudioPlayer!
    var btnSound2 : AVAudioPlayer!
    var runningNumber = ""
    
    enum Operation: String{
        case Divide = "/"
        case Multiply = "*"
        case Add = "+"
        case Subtract = "-"
        case Empty = "Empty"
    }
    
    var currentOperation = Operation.Empty
    var leftValStr = ""
    var rightValStr = ""
    var result = ""
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let path = Bundle.main.path(forResource: "btn", ofType: "wav")
        let soundURL = URL(fileURLWithPath: path!)
        
        let path2 = Bundle.main.path(forResource: "btn 2", ofType: "wav")
        let soundURL2 = URL(fileURLWithPath: path2!)
        
        outputLbl.text = "0"
        
        do {
            try btnSound2 = AVAudioPlayer(contentsOf: soundURL2)
        } catch let err as NSError {
            print(err.debugDescription)
        }
        
        do {
            try btnSound = AVAudioPlayer(contentsOf: soundURL)
        } catch let err as NSError {
            print(err.debugDescription)
        }
    }
    
    @IBAction func numberPressed2 (sender : UIButton){
        playSound2()
    }
    
    
    @IBAction func numberPressed (sender : UIButton){
        playSound()
        runningNumber += ("\(sender.tag)")
        outputLbl.text = runningNumber
    }
    
    @IBAction func onDividePressed(sender: AnyObject){
    processOperation(operation: .Divide)
    }
    
    @IBAction func onMultiplyPressed(sender: AnyObject){
    processOperation(operation: .Multiply)
    }
    
    @IBAction func onSubtractPressed(sender: AnyObject){
    processOperation(operation: .Subtract)
    }
    
    @IBAction func onAddPressed(sender: AnyObject){
    processOperation(operation: .Add)
    }
    
    @IBAction func onEqualPressed(sender: AnyObject){
    processOperation(operation: currentOperation)
    }
    
    func playSound2(){
        if btnSound2 .isPlaying{
            btnSound2.stop()
        }
        btnSound2.play()
    }
    
    
    
    func playSound(){
        if btnSound .isPlaying{
            btnSound.stop()
        }
        btnSound.play()
    }
    
    func processOperation (operation : Operation){
    
        if currentOperation != Operation.Empty{
            if runningNumber != ""{
                rightValStr = runningNumber
                runningNumber = ""
                
                if currentOperation == Operation.Multiply{
                    result = "\(Double(leftValStr)! * Double(rightValStr)!)"
                }
                
                else if currentOperation == Operation.Divide{
                    result = "\(Double(leftValStr)! / Double(rightValStr)!)"
                }
                else if currentOperation == Operation.Add{
                result = "\(Double(leftValStr)! + Double(rightValStr)!)"
                }
                else if currentOperation == Operation.Subtract{
                result = "\(Double(leftValStr)! - Double(rightValStr)!)"
                }
                
                leftValStr = result
                outputLbl.text = result
            }
            
            currentOperation = operation
        }else {
        //this is the first time an operator has been pressed
            leftValStr = runningNumber
            runningNumber = ""
            currentOperation = operation
        
        }
        
    }

}

