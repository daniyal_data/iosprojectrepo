//
//  PartyCellTVC.swift
//  SongSation
//
//  Created by Huzaifa Kausar on 7/21/17.
//  Copyright © 2017 Huzaifa Kausar. All rights reserved.
//

import UIKit
import Foundation

class PartyCellTVC: UITableViewCell {

    @IBOutlet weak var videoPreviewImage: UIImageView!
    
    @IBOutlet weak var videoTitle: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func updateUI(partyRock : PartyRock){
        videoTitle.text = partyRock.videoTitle
        //TODO: set image from url
        let url = URL(string : partyRock.imageURL)!
        DispatchQueue.global().async {
            do{
                let data = try Data(contentsOf: url)
                DispatchQueue.global().sync {
                    self.videoPreviewImage = UIImage(data: data)
                }
                
            }catch{
                //handle the error
            }
        }
    }
}
