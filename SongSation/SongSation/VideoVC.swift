//
//  VideoVC.swift
//  SongSation
//
//  Created by Huzaifa Kausar on 7/20/17.
//  Copyright © 2017 Huzaifa Kausar. All rights reserved.
//

import UIKit

class VideoVC: UIViewController {
    @IBOutlet weak var webView : UIWebView!
    
    @IBOutlet weak var titleLbl: UILabel!
    
    private var _partyRock : PartyRock!
    get {
    return _partyRock
    }set{
    _partyRock = newValue
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        titleLbl.text = _partyRock.videoTitle
        webView.loadHTMLString(_partyRock.videoURL, baseURL: nil)
    
       
    }

    

}
