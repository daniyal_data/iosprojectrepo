//
//  ViewController.swift
//  SongSation
//
//  Created by Huzaifa Kausar on 7/20/17.
//  Copyright © 2017 Huzaifa Kausar. All rights reserved.
//

import UIKit

class MainVC: UIViewController , UITableViewDelegate, UITableViewDataSource{
    
    @IBOutlet weak var tableView : UITableView!
    

    var partyRocks = [PartyRock]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let p1 = PartyRock(imageURL: "https://i.ytimg.com/vi/Umqb9KENgmk/hqdefault.jpg?sqp=-oaymwEXCPYBEIoBSFryq4qpAwkIARUAAIhCGAE=&rs=AOn4CLChhdnAtEcX0c3mFtysy_0kRSjRaQ", videoURL: "<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/Umqb9KENgmk\" frameborder=\"0\" allowfullscreen></iframe>", videoTitle: "Tum Hi Ho Ashiqui")
        
        partyRocks.append(p1)
        
        tableView.delegate = self
        tableView.dataSource = self
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCell(withIdentifier: "PartyCellTVC", for: <#T##IndexPath#>) as? PartyCellTVC{
            let partyRock = partyRocks[indexPath.row]
            cell.updateUI(partyRock: partyRock)
            
            return cell
            
        }else {
            return UITableViewCell()
        }
        
        return UITableViewCell()
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let partyRock = partyRocks[indexPath.row]
        performSegue(withIdentifier: "VideoVC", sender: partyRock)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destination = segue.destination as? VideoVC{
            if let party = sender as? PartyRock{
                destination.partyRock = party
            }
        }

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return partyRocks.count
    }
}

