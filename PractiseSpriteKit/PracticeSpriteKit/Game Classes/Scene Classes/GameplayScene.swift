//
//  GameplayScene.swift
//  PracticeSpriteKit
//
//  Created by Huzaifa Kausar on 12/20/17.
//  Copyright © 2017 Huzaifa Kausar. All rights reserved.
//

import SpriteKit
import GameplayKit

class GameplayScene : SKScene{
    var player : SKSpriteNode!
    
    override func didMove(to view: SKView) {
        print("this scene was loaded")
        player = SKSpriteNode(fileNamed: "player")
        player.position = CGPoint(x: self.frame.size.width / 2, y: player.size.height / 2 + 20)
        
        self.addChild(player)
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
      
    }
    
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
    }
    
    
    
    override func update(_ currentTime: TimeInterval) {
        
        
    }
}
