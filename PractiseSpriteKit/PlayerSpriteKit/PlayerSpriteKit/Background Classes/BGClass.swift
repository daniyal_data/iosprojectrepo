//
//  BGClass.swift
//  PlayerSpriteKit
//
//  Created by Huzaifa Kausar on 1/1/18.
//  Copyright © 2018 Huzaifa Kausar. All rights reserved.
//

import SpriteKit

class BGClass : SKSpriteNode{
    
    func moveBG(camera : SKCameraNode){
        if self.position.y - self.size.height - 10 > camera.position.y{
            self.position.y += self.size.height * 3
        }
    }
}
