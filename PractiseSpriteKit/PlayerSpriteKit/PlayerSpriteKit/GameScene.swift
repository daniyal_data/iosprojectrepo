//
//  GameScene.swift
//  PlayerSpriteKit
//
//  Created by Huzaifa Kausar on 12/20/17.
//  Copyright © 2017 Huzaifa Kausar. All rights reserved.
//

import SpriteKit
import GameplayKit
import CoreMotion

class GameScene: SKScene, SKPhysicsContactDelegate {
    var mainCamera: SKCameraNode?
    var bg1 : BGClass?
    var bg2 : BGClass?
    var bg3 : BGClass?
    var player: SKSpriteNode!
    let motionManager = CMMotionManager()
    var xAcceleration: CGFloat = 0
    
    override func didMove(to view: SKView) {
        player = SKSpriteNode(imageNamed: "player")
        player.xScale = 0.3
        player.yScale = 0.5
        mainCamera = self.childNode(withName: "Main Camera") as? SKCameraNode!
        getBackgrounds()
        self.addChild(player)
        self.physicsWorld.gravity = CGVector(dx: 0, dy: 0)
        self.physicsWorld.contactDelegate = self
        
        motionManager.accelerometerUpdateInterval = 0.2
        motionManager.startAccelerometerUpdates(to: OperationQueue.current!) { (data:CMAccelerometerData?, error:Error?) in
            if let accelerometerData = data{
                let acceleration = accelerometerData.acceleration
                self.xAcceleration = CGFloat(acceleration.x) * 0.75 + self.xAcceleration * 0.25
                
            }
        }
    }
    override func didSimulatePhysics() {
        player.position.x += xAcceleration * 50
       
        if player.position.x < -20 {
            player.position = CGPoint(x: self.size.width + 20, y: player.position.y)
        }else if player.position.x > self.size.width + 20{
            player.position = CGPoint(x: -20, y: player.position.y)
        }
    }
    
    func moveCamera(){
        self.mainCamera?.position.y += 3
    }
    
    func getBackgrounds(){
        bg1 = self.childNode(withName: "BG 1") as? BGClass!
        bg2 = self.childNode(withName: "BG 2") as? BGClass!
        bg3 = self.childNode(withName: "BG 3") as? BGClass!
    }
    
    func movePlayer(){
        self.player.position.y += 3
    }
    func manageBackgrounds(){
        bg1?.moveBG(camera: mainCamera!)
        bg2?.moveBG(camera: mainCamera!)
        bg3?.moveBG(camera: mainCamera!)
    }
    
    override func update(_ currentTime: TimeInterval) {
        // Called before each frame is rendered
        moveCamera()
        movePlayer()
        manageBackgrounds()
    }
}
